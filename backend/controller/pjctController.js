var Pjct = require('../model/pjct');
var PjctMap = require('../model/pjct-map');
exports.pjctDetails = function(req, res) {
    Pjct.get(function (err, pjct) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Pjct details retrieved successfully",
            data: pjct
        });
    });
};

exports.pjctMapping = function(req,res){
    PjctMap.save(req,function (err) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Pjct details inserted successfully"
        });
    })
}