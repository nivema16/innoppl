var Login = require('../model/login');

exports.loginDetails = function(req, res) {
    Login.get(req,function (err, login) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Login details retrieved successfully",
            data: login
        });
    });

        
};