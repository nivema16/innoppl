var mongoose = require("mongoose");
// Setup schema
var pjctMapSchema = mongoose.Schema({
  pjct_id: String,
  emp_id: String,
});

var Pjct = (module.exports = mongoose.model("projectmappings", pjctMapSchema));
module.exports.save = function (req, callback) {
  var insertList = [];

  req.body.pjcts.forEach((element) => {
    let ip = {};
    ip.emp_id = req.body.empData.id;
    ip.pjct_id = element.id;
    insertList.push(ip);
  });

  Pjct.insertMany(insertList)
    .then(function () {
      callback(null);
    })
    .catch(function (error) {
      callback(error); // Failure
    });
};

module.exports.empDetails = async function (employee, callback) {
  Pjct.aggregate([
    {
      $match: { emp_id: { $in: employee } },
    },
    {
      $group: {
        _id: "$emp_id",
        emp_id: { $first: "$emp_id" },
        pjct: { $push: "$pjct_id" },
      },
    },
  ]).then((data) => {
    callback(null, data);
  });
};
