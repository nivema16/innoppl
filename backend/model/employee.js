
var mongoose = require('mongoose');
// Setup schema
var employeeSchema = mongoose.Schema({
    name: String,
    id: String,
    gender: String
});

var Employee = module.exports = mongoose.model('employees', employeeSchema);
module.exports.get = function (callback) {
    Employee.find({},(err,data)=>{
        callback(err,data);
    });
}