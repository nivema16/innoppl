var mongoose = require("mongoose");
// Setup schema
var pjctSchema = mongoose.Schema({
  name: String,
  id: String,
});

var Pjct = (module.exports = mongoose.model("projects", pjctSchema));
module.exports.get = function (callback) {
  Pjct.find({}, (err, data) => {
    callback(err, data);
  });
};

module.exports.getNames = function (pjctIds, callback) {
  return Pjct.find({ id: { $in: pjctIds } });
};
